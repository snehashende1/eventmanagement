public class Eventcontroller { // use case - EventController
    @AuraEnabled
    public static Campaign saveEvent(Campaign campaign) {
        if (campaign == null) {
            return null;
        }
        
        try {
            update campaign;
        } catch(Exception ex) {
            
        }
        
        return campaign;
    }
    
    @AuraEnabled
    public static Campaign getcampign(Id recordId) { // change name recordId to campaignId
        Campaign campaign = [ SELECT Name
                                   , IsActive
                                   , Type__c
                                   , All_Day_Event__c 
                                   , Start_Date__c
                                   , End_Date__c
                                   , Description 
                                FROM Campaign 
                               WHERE id= :recordId
        ];
        
        return campaign;
    }
    
    @AuraEnabled
    public static List<String> getPickListValue() {
        
        List<String> campaignTypeList = new List<String>();
        
        Schema.DescribeFieldResult fieldResult = Campaign.Type__c.getDescribe();
        List<Schema.PicklistEntry> picklistValues = fieldResult.getPicklistValues();
        
        for (Schema.PicklistEntry value: picklistValues) {
            campaignTypeList.add(value.getLabel());
        }
        
        return campaignTypeList;
    }
}